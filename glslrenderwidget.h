#ifndef GLSLRENDERWIDGET_H
#define GLSLRENDERWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QPair>
#include <QVector2D>

class QOpenGLVertexArrayObject;
class QOpenGLBuffer;
class QOpenGLShaderProgram;
class QTime;

class GLSLRenderWidget :
        public QOpenGLWidget,
        protected QOpenGLFunctions
{
    Q_OBJECT
    Q_DISABLE_COPY(GLSLRenderWidget)
public:
    explicit GLSLRenderWidget();
    ~GLSLRenderWidget() Q_DECL_OVERRIDE;

public slots:
    void setGLSLCode(const QString& f_shader);
    void restartTimer();
    void setRenderMode(bool mode);

protected:
    void initializeGL() Q_DECL_OVERRIDE;
    void resizeGL(int w,int h) Q_DECL_OVERRIDE;
    void paintGL() Q_DECL_OVERRIDE;

    void timerEvent(QTimerEvent *event) Q_DECL_OVERRIDE;

private:
    QOpenGLVertexArrayObject *m_vao;
    QOpenGLBuffer            *m_vbo;
    QOpenGLBuffer            *m_ibo;

    QOpenGLShaderProgram     *m_program;
    QString                   m_fShaderCode;
    QString                   m_vShaderCode;
    QPair<int,int>            m_version;
    QString                   m_versionStr;

    QVector2D                 m_resolution;
    QTime                    *m_time;
    int                       m_timerId;


signals:
    void changeGLSLCode(const QString& str);
};

#endif // GLSLRENDERWIDGET_H

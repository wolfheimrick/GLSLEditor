#include "glslcodewidget.h"
#include <QPainter>
#include <QTextBlock>
#include <QFontDatabase>

/*
 * See below site
 * http://doc.qt.io/qt-5/qtwidgets-widgets-codeeditor-example.html
 */
GLSLCodeWidget::GLSLCodeWidget(QWidget *parent) :
    QPlainTextEdit (parent)
{
    m_lineNumber = new LineNumberArea(this);

    connect(this,&QPlainTextEdit::blockCountChanged,
            this,&GLSLCodeWidget::updateLineAreaWidth);
    connect(this,&QPlainTextEdit::updateRequest,
            this,&GLSLCodeWidget::updateLineArea);
    updateLineAreaWidth(0);

    setWordWrapMode(QTextOption::NoWrap);

    QFont font = QFontDatabase().font("Monaco","Osaka",16);
    setFont(font);

    setMinimumWidth(500);
}

int GLSLCodeWidget::lineAreaWidth(){

    int digit = 1;
    int max = qMax(1,blockCount());
    while(max >= 10){
        max /= 10;
        digit ++;
    }

    int space = fontMetrics().averageCharWidth() * (digit + 4);
    return space;
}

void GLSLCodeWidget::lineAreaPaintEvent(QPaintEvent *event){
    QPainter painter(m_lineNumber);
    const QRect r = event->rect();
    painter.fillRect(QRect(r.x(),r.y(),r.width()*4/5,r.height()),Qt::lightGray);

    QTextBlock block = firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
    int bottom = top + (int) blockBoundingRect(block).height();

    while (block.isValid() && top <= event->rect().bottom()) {
        if (block.isVisible() && bottom >= event->rect().top()) {
            QString number = QString::number(blockNumber + 1) + QString("  ");
            painter.setPen(Qt::black);
            painter.drawText(0, top, m_lineNumber->width(), fontMetrics().height(),
                             Qt::AlignRight, number);
        }

        block = block.next();
        top = bottom;
        bottom = top + (int) blockBoundingRect(block).height();
        ++blockNumber;
    }
}

void GLSLCodeWidget::updateLineAreaWidth(int){
    setViewportMargins(lineAreaWidth(),0,0,0);
}

void GLSLCodeWidget::updateLineArea(const QRect &rect, int dy){

    if (dy){
        m_lineNumber->scroll(0, dy);
    }else{
        m_lineNumber->update(0, rect.y(), m_lineNumber->width(), rect.height());
    }

    if (rect.contains(viewport()->rect())){
        updateLineAreaWidth(0);
    }

}
void GLSLCodeWidget::resizeEvent(QResizeEvent *event){

    QPlainTextEdit::resizeEvent(event);

    const QRect cr = contentsRect();

    m_lineNumber->setGeometry(QRect(cr.x(),cr.y(),lineAreaWidth(),cr.height()));
}


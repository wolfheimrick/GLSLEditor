#include "glslcontrolwidget.h"
#include "glslcodewidget.h"

#include <QPushButton>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include <QtDebug>
GLSLControlWidget::GLSLControlWidget(QWidget *parent) :
    QWidget (parent)
{
    m_editor             = new GLSLCodeWidget;
    m_restartButton      = new QPushButton("Restart");
    m_renderModeCheckBox = new QCheckBox("Always");
    m_utilWidget         = new QWidget;
    m_hlayout            = new QHBoxLayout;
    m_vlayout            = new QVBoxLayout;

    m_vlayout->addWidget(m_editor);
    m_vlayout->addWidget(m_utilWidget);

    m_hlayout->addWidget(m_renderModeCheckBox);
    m_hlayout->addWidget(m_restartButton);

    m_utilWidget->setLayout(m_hlayout);
    setLayout(m_vlayout);

    connect(m_editor,&QPlainTextEdit::textChanged,
            this,&GLSLControlWidget::analyzeGLSLCode);

    connect(m_restartButton,&QPushButton::clicked,
            [&]{
        emit restart();
    });

    connect(m_renderModeCheckBox,&QCheckBox::clicked,
            [&](bool mode){
        emit setRenderMode(mode);
    });
}

GLSLControlWidget::~GLSLControlWidget(){
}

void GLSLControlWidget::setGLSLCode(const QString& code){
    m_editor->setPlainText(code);
}

void GLSLControlWidget::analyzeGLSLCode(){
    emit changeGLSLCode(m_editor->toPlainText());
}

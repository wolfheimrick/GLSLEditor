#ifndef GLSLCONTROLWIDGET_H
#define GLSLCONTROLWIDGET_H

#include <QWidget>

class GLSLCodeWidget;
class QPushButton;
class QCheckBox;
class QHBoxLayout;
class QVBoxLayout;

class GLSLControlWidget : public QWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(GLSLControlWidget)
public:
    GLSLControlWidget(QWidget *parent = Q_NULLPTR);
    ~GLSLControlWidget();

public slots:
    void setGLSLCode(const QString& code);
    void analyzeGLSLCode();

signals:
    void changeGLSLCode(const QString&);
    void restart();
    void setRenderMode(bool);

private:
    GLSLCodeWidget   *m_editor;

    QPushButton      *m_restartButton;
    QCheckBox        *m_renderModeCheckBox;
    QWidget          *m_utilWidget;
    QHBoxLayout      *m_hlayout;
    QVBoxLayout      *m_vlayout;
};

#endif // GLSLCONTROLWIDGET_H
